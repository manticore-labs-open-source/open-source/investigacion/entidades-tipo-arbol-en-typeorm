# Entidades tipo arbol (Tree) en typeorm
La lista de adyacencia es un modelo simple con autorreferencia. El beneficio de este enfoque es la simplicidad, el inconveniente es que no puede cargar grandes árboles de una sola vez debido a las limitaciones de la unión. Ejemplo:

![](./imagenes/Selección_130.png)


## Conjunto anidado
El conjunto anidado es otro patrón de almacenamiento de estructuras de árbol en la base de datos. Es muy eficiente para las lecturas, pero malo para las escrituras. No puedes tener múltiples raíces en un conjunto anidado. Ejemplo:

![](./imagenes/Selección_131.png)

## Ruta materializada 
La ruta materializada (también llamada enumeración de ruta) es otro patrón de almacenamiento de estructuras de árbol en la base de datos. Es simple y eficaz. Ejemplo:

![](./imagenes/Selección_132.png)

## Tabla de cierre
La tabla de cierre almacena las relaciones entre padres e hijos en una tabla separada de una manera especial. Es eficiente tanto en lecturas como en escrituras. Ejemplo:

![](./imagenes/Selección_133.png)

## Trabajando con entidades arbóreas.
Para que las entidades de árbol se unan entre sí, es importante establecer las entidades secundarias como sus principales y guardarlas, por ejemplo:

![](./imagenes/Selección_134.png)

como podemos ver se han creado dos tablas, una donde se guardan el id del nodo padre e hijo:

![](./imagenes/Selección_135.png)


y otra donde se guardan los datos de la entidad.

![](./imagenes/Selección_136.png)


Para cargar dicho árbol use TreeRepository:

![](./imagenes/Selección_137.png)



Hay otros métodos especiales para trabajar con entidades de árbol como TreeRepository:

* findTrees - Devuelve todos los árboles en la base de datos con todos sus hijos, hijos de niños, etc.

![](./imagenes/Selección_138.png)

![](./imagenes/Selección_139.png)

* findRoots: las raíces son entidades que no tienen ancestros.

![](./imagenes/Selección_140.png)

![](./imagenes/Selección_141.png)

* findDescendants - Obtiene todos los hijos (descendientes) de la entidad dada. Los devuelve todos en una matriz plana.

![](./imagenes/Selección_142.png)

![](./imagenes/Selección_143.png)


<a href="https://www.facebook.com/oscar.sambache" target="_blank"><img alt="Encuentrame en facebook:" height="35" width="35" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTANMlMcb_-pUSuzjnpvSynOA3Ontg9Z2I1NE24WQ_KAk34yYmh"/> Oscar Sambache</a>


